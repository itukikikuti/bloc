{-# LANGUAGE ScopedTypeVariables #-}
module Interactive where

import System.IO

input :: IO String
input = do
    putStr ">"
    hFlush stdout
    getLine
    
interactive :: IO ()
interactive = do
    putStrLn =<< input
