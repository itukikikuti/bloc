module Parser where

import Lexer

data Expression =
    LiteralExpression Literal |
    InfixExpression InfixOperator Expression Expression
    deriving (Show, Eq)

data Literal =
    IntLiteral Int
    deriving (Show, Eq)

data InfixOperator =
    AddOperator |
    SubOperator |
    MulOperator |
    DivOperator
    deriving (Show, Eq)

parse :: [Token] -> Expression
parse (token:tokens) = parseExpression token

parseExpression:: Token -> Expression
parseExpression (IntLiteralToken value) =
    LiteralExpression (IntLiteral value)
parseExpression token
    | match token [PlusToken, MinusToken, AsteriskToken, SlashToken] =
        LiteralExpression (IntLiteral 0)
    | otherwise = LiteralExpression (IntLiteral 99999)
