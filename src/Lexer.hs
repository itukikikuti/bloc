module Lexer where

data Token =
    UnknownToken |
    IntLiteralToken Int |
    PlusToken |
    MinusToken |
    AsteriskToken |
    SlashToken
    deriving (Show, Eq)

lex :: String -> [Token]
lex source = map tokenize (split (removeWhiteSpace source) ',')

tokenize :: String -> Token
tokenize [] = UnknownToken
tokenize "+" = PlusToken
tokenize "-" = MinusToken
tokenize "*" = AsteriskToken
tokenize "/" = SlashToken
tokenize string
    | isInt string = IntLiteralToken (toInt string)
    | otherwise = UnknownToken

isInt :: String -> Bool
isInt string =
    not (match False bools)
    where
        bools = map (\x -> match x ['0'..'9']) string

toInt :: String -> Int
toInt string = read string :: Int

removeWhiteSpace :: String -> String
removeWhiteSpace source =
    map (\x -> replace x [' ', '\t', '\n', '\r'] ',') source

replace :: Char -> [Char] -> Char -> Char
replace char [] newChar = char
replace char (oldChar:candidates) newChar
    | char == oldChar = newChar
    | otherwise = replace char candidates newChar

match :: Eq a => a -> [a] -> Bool
match _ [] = False
match value (candidate:candidates)
    | value == candidate = True
    | otherwise = match value candidates

split :: String -> Char -> [String]
split [] _ = []
split string separator =
    x:split (drop 1 y) separator where (x, y) = span (/= separator) string
