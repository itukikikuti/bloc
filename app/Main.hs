module Main where

import Prelude hiding (lex)
import Lexer
import Parser

main :: IO ()
main = do
    source <- readFile "test/Test1.box"
    print (eval source)

eval :: String -> Expression
eval source = expression
    where
        tokens = lex source
        expression = parse tokens
